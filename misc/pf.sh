#!/bin/bash
#!if install_postfix

echo "postfix postfix/main_mailer_type select Internet Site" | debconf-set-selections
echo "postfix postfix/mailname string ASK_HOSTNAME" | debconf-set-selections
apt-get -y install postfix

postconf -e 'header_checks = regexp:/etc/postfix/header_checks'

echo '/^Received: by .*Postfix, from userid [0-9]+\)/ IGNORE' >> /etc/postfix/header_checks

echo "inet_protocols = ipv4" >> /etc/postfix/main.cf

service postfix restart

echo tech.support@example.com > /root/.forward
echo ASK_$FN | sendmail root

#!endif install_postfix
